package com.mephiPP.dictionary;

import java.util.List;

// из того интерфейса можно получать лексемы группами
public interface LexemeProvider {
    List<Lexeme> extractLexemes(int num);
}
