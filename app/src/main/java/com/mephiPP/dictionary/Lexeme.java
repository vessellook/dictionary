package com.mephiPP.dictionary;

import java.util.List;

public class Lexeme {
    private int id;
    private String word;
    private List<String> translations;
    private List<String> definitions;
    private Lang nativeLang;
    private Lang targetLang;

    public Lexeme(int id,
                  String word,
                  List<String> translations,
                  List<String> definitions,
                  Lang nativeLang,
                  Lang targetLang) {
        this.id = id;
        this.word = word;
        this.translations = translations;
        this.definitions = definitions;
        this.nativeLang = nativeLang;
        this.targetLang = targetLang;
    }

    public int getId() { return id; }

    public String getWord() {
        return word;
    }

    public List<String> getTranslations() {
        return translations;
    }

    public List<String> getDefinitions() {
        return definitions;
    }

    public Lang getNativeLang() {
        return nativeLang;
    }

    public Lang getTargetLang() {
        return targetLang;
    }
}
