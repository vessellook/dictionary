package com.mephiPP.dictionary;

import java.util.LinkedList;
import java.util.List;

public class LexemeManager {
    // реализация паттерна Singleton
//
//    private LexemeManager() {
//    }
//
//    static private LexemeManager singleton = null;
//
//    static public LexemeManager instance() {
//        if (singleton == null) singleton = new LexemeManager();
//        return singleton;
//    }
    // TODO: заглушка
    public LexemeProvider getWordsInAlphabetOrder(Lang nativeLang, Lang targetLang) {
        return new StubLexemeProvider();
    }

    // TODO: заглушка
    public LexemeProvider getWordsInRandomOrder(Lang nativeLang, Lang targetLang) {
        return new StubLexemeProvider();
    }
}
