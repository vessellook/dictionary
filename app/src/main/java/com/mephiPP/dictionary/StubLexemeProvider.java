package com.mephiPP.dictionary;

import java.util.LinkedList;
import java.util.List;

// класс-заглушка
public class StubLexemeProvider implements LexemeProvider {
    @Override
    public List<Lexeme> extractLexemes(int num) {
        LinkedList<Lexeme> list = new LinkedList<>();
        list.add(getApple());
        list.add(getWinter());
        list.add(getBoard());
        return list;
    }

    // для заглушек
    private Lexeme getApple() {
        LinkedList<String> translations = new LinkedList<>();
        LinkedList<String> definitions = new LinkedList<>();
        translations.add("яблоко");
        definitions.add("a hard, round fruit with a green or red skin");
        return new Lexeme(0,"apple", translations, definitions, Lang.RUSSIAN, Lang.ENGLISH);
    }

    // для заглушек
    private Lexeme getBoard() {
        LinkedList<String> translations = new LinkedList<>();
        LinkedList<String> definitions = new LinkedList<>();
        translations.add("доска");
        definitions.add("a long, thin, flat piece of wood");

        translations.add("доска");
        definitions.add("a flat piece of wood, plastic, etc used for a particular purpose");

        translations.add("доска объявлений");
        definitions.add("a piece of wood, plastic, etc on a wall, where information can be put");

        translations.add("классная доска");
        definitions.add("a surface on the wall of a school room that the teacher writes on");

        translations.add("доска для настольной игры");
        definitions.add("a piece of wood, cardboard, etc for playing games on");

        translations.add("доска для настольной игры");
        definitions.add("a group of people who officially control a company or organization,"
                + " or a particular type of business activity");

        translations.add("питание");
        definitions.add("meals that are provided when you stay in a hotel");

        return new Lexeme(0,"board", translations, definitions, Lang.RUSSIAN, Lang.ENGLISH);
    }

    // для заглушек
    private Lexeme getWinter() {
        LinkedList<String> translations = new LinkedList<>();
        LinkedList<String> definitions = new LinkedList<>();
        translations.add("зима");
        definitions.add("the coldest season of the year, between autumn and spring");
        return new Lexeme(0,"winter", translations, definitions, Lang.RUSSIAN, Lang.ENGLISH);
    }
}
