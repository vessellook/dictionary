package com.mephiPP.dictionary;

public class DatabaseDownloader {
    final private Lang nativeLang;
    final private Lang targetLang;

    public DatabaseDownloader(Lang nativeLang, Lang targetLang) {
        this.nativeLang = nativeLang;
        this.targetLang = targetLang;
    }

    // TODO: этот метод должен по данным языкам сохранить базу данных с сервера по пути "path"
    public void downloadDatabase(String path) {
        // можно ещё возвращать флаг об успехе операции
    }
}
